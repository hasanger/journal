var journalID = new URLSearchParams(window.location.search).get("id");

$(document).ready(function() {
	$("#newEntryBtn").click(function() {
		$.post("/api/v1/journal/" + journalID + "/newEntry", function(entryID) {
			window.location.replace("/edit?journal=" + journalID + "&entry=" + entryID);
		});
	});

	$.get("/api/v1/journal/" + journalID, function(journal) {
		$("#journalTitle").html(journal.title);
		$("#journalInfo").html(journal.entryCount + (journal.entryCount == 1 ? " entry" : " entries") + " ● Last updated " + journal.lastUpdatedText);
	});

	$.get("/api/v1/journal/" + journalID + "/entries", function(data) {
		if(data.length == 0) $("#entryList").hide();
		data.forEach(function(entry) {
			var newItem = $(`
							<a class="journal-link" href="/edit?journal=${journalID}&entry=${entry.uniqueID}">
							  <li class="list-group-item">
					            ${entry.title.trim().length == 0 ? "(no title)" : entry.title}<br>
					            <small>${entry.dateText}</small>
					          </li>
							</a>
							`);
			$("#entryList").append(newItem);
		});
	});
});
