$(document).ready(function() {
	$("#newJournalBtn").click(function() {
		var title = $("#newJournalTitleField").val();
		if(title.length == 0 || title.trim() === "") return;
		$("#newJournalTitleField").val("");
		$.ajax({
	        url: "/api/v1/newJournal",
	        type: "POST",
	        data: title,
	        contentType: "application/json"
		}).done(function() {
			$("#journalList").html("");
			getJournals();
		});
	});
	getJournals();
});

function getJournals() {
	$.get("/api/v1/journals", function(data) {
		$("#journalCount").html(data.length + (data.length == 1 ? " journal" : " journals"));
		data.forEach(function(journal) {
			var newItem = $(`
							<a class="journal-link" href="/journal?id=${journal.uniqueID}">
							  <div id="${journal.uniqueID}" class="card">
						 	    <h1>${journal.title}</h1>
						 	    <small>${journal.entryCount} ${journal.entryCount == 1 ? "entry" : "entries"} ● Last updated ${journal.lastUpdatedText}</small>
							  </div>
							</a>
							`);
			$("#journalList").append(newItem);
		});
	});
}