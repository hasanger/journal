var searchParams = new URLSearchParams(window.location.search);
var journalID = searchParams.get("journal");
var saved = true;

tinymce.init({
    selector: "#writingArea",
    menubar: false,
    statusbar: false,
    nowrap: false,
    skin: "dark",
	content_css: "dark",
    forced_root_block: "div",
    save_onsavecallback: save,
    save_enablewhendirty: false,
    plugins: "save searchreplace lists image link",
    toolbar: "save | undo redo | fontfamily fontsize | bold italic underline strikethrough | bullist numlist | image link hr | backcolor forecolor | indent outdent | alignleft aligncenter alignright alignjustify | removeformat",
    font_family_formats: "Open Sans=open sans,sans-serif; Andale Mono=andale mono,times; Arial=arial,helvetica,sans-serif; Arial Black=arial black,avant garde; Book Antiqua=book antiqua,palatino; Comic Sans MS=comic sans ms,sans-serif; Courier New=courier new,courier; Georgia=georgia,palatino; Helvetica=helvetica; Impact=impact,chicago; Symbol=symbol; Tahoma=tahoma,arial,helvetica,sans-serif; Terminal=terminal,monaco; Times New Roman=times new roman,times; Trebuchet MS=trebuchet ms,geneva; Verdana=verdana,geneva; Webdings=webdings; Wingdings=wingdings,zapf dingbats",
    setup: function(editor) {
    	editor.on("change input", function() {
    		saved = false;
    		$("button[title='Save'] path").attr("fill", "#dc3545");
    	});
    }
});

$(document).ready(function() {
	$("#writingCol").css("visibility", "hidden");
	setTimeout(function() {
		$.get("/api/v1/journal/" + searchParams.get("journal") + "/entry/" + searchParams.get("entry"), function(entry) {
			var now = new Date();
		    var month = (now.getMonth() + 1);               
		    var day = now.getDate();
		    if (month < 10) 
		        month = "0" + month;
		    if (day < 10) 
		        day = "0" + day;
		    var today = now.getFullYear() + '-' + month + '-' + day;
			$(".tox-editor-container").prepend($(`
												 <div>
												   <input id="entryTitle" placeholder="Entry Title">
												   <div id="entryOptions">
												     <div id="entryDateContainer">
												   	   <i class="bi bi-calendar2"></i>
												       <input id="entryDate" type="date" value="${today}">
												     </div>
												     <div class="dropdown">
												     	<button id="moreOptionsBtn" class="btn dropdown-toggle" data-bs-toggle="dropdown"><i class="bi bi-three-dots-vertical"></i></button>
												     	<ul class="dropdown-menu" aria-labelledby="moreOptionsBtn">
												          <li><a id="deleteEntryBtn" class="dropdown-item"><i class="bi bi-trash"></i> Delete entry</a></li>
												        </ul>
												     </div>
												   </div>
												 </div>
												 `));
			$("#entryDateContainer").click(function() {
				document.querySelector("#entryDate").showPicker();
			});
			$("#entryTitle").val(entry.title);
			tinymce.activeEditor.setContent(entry.content);
			tinymce.activeEditor.undoManager.clear();
			document.querySelector("#entryDate").valueAsDate = new Date(entry.date);
			$("#entryTitle, #entryDate").on("input", function() {
				$("button[title='Save'] path").attr("fill", "#dc3545");
				saved = false;
			});
			$("#deleteEntryBtn").click(function() {
				var confirmation = confirm("Are you sure you want to delete the entry? This cannot be undone.");
				if(confirmation == true) {
					$.ajax({
						url: "/api/v1/journal/" + searchParams.get("journal") + "/entry/" + searchParams.get("entry") + "/delete",
						type: "DELETE"
					});
					window.location.replace("/journal?id=" + journalID);
				}
			});
			$("#writingCol").hide();
			$("#writingCol").css("visibility", "inherit");
			$("#writingCol").fadeIn(500);
		});
	}, 200);

	

	$("#newEntryBtnWide").click(function() {
		$.post("/api/v1/journal/" + journalID + "/newEntry", function(entryID) {
			window.location.replace("/edit?journal=" + journalID + "&entry=" + entryID);
		});
	});

	$("#journalTitle, #allEntriesBtn").attr("href", "/journal?id=" + journalID);

	$("#randomEntryBtn").click(function() {
		$.get("/api/v1/journal/" + journalID + "/randomEntry", function(entry) {
			window.location.replace("/edit?journal=" + journalID + "&entry=" + entry.uniqueID);
		});
	});

	$.get("/api/v1/journal/" + journalID, function(journal) {
		$("#journalTitle").html(journal.title);
		$("#journalInfo").html(journal.entryCount + (journal.entryCount == 1 ? " entry" : " entries") + " ● Last updated " + journal.lastUpdatedText);
	});

	getEntries();

	$(window).on("beforeunload", function(e) {
		if(!saved) {
			e.preventDefault();
			e.returnValue = "";
			return "";
		}
	});

	$("body").on("keydown", function(e) {
		if(e.ctrlKey && e.key === 's') {
			e.preventDefault();
			$('button[title="Save"]').click();
		}
	});
});

function save() {
	$.ajax({
		url: "/api/v1/journal/" + searchParams.get("journal") + "/entry/" + searchParams.get("entry") + "/edit",
		type: "PATCH",
		data: JSON.stringify({
			title: $("#entryTitle").val(),
			content: tinymce.activeEditor.getContent(),
			date: $("#entryDate").val()
		}),
		success: function(data) {
			$("button[title='Save'] path").removeAttr("fill");
			saved = true;
			$("#entryList").html("");
			getEntries();
		}
	});
}

function getEntries() {
	$.get("/api/v1/journal/" + journalID + "/entries/10", function(data) {
		data.forEach(function(entry) {
			var newItem = $(`
							<a class="journal-link" href="/edit?journal=${journalID}&entry=${entry.uniqueID}">
							  <li class="list-group-item">
					            ${entry.title.trim().length == 0 ? "(no title)" : entry.title}<br>
					            <small>${entry.dateText}</small>
					          </li>
							</a>
							`);
			$("#entryList").append(newItem);
		});
	});
}
